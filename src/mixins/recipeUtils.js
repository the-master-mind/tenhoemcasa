const SWEET_CATEGORY_DEFAULT_IMAGE = '/statics/sweet_default.png';
const SALTY_CATEGORY_DEFAULT_IMAGE = '/statics/salty_default.png';
const FITNESS_CATEGORY_DEFAULT_IMAGE = '/statics/fitness_default.png';
const NO_CATEGORY_DEFAULT_IMAGE = '/statics/food.png';

module.exports = {
  methods: {
    recipeIconByCategory: (category) => {
      let icon = '';
      switch (category) {
        case 'sweet':
          icon = SWEET_CATEGORY_DEFAULT_IMAGE;
          break;
        case 'salty':
          icon = SALTY_CATEGORY_DEFAULT_IMAGE;
          break;
        case 'fitness':
          icon = FITNESS_CATEGORY_DEFAULT_IMAGE;
          break;
        default:
          icon = NO_CATEGORY_DEFAULT_IMAGE;
          break;
      }
      return icon;
    },
  },
};
