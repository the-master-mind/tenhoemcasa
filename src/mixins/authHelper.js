import { Notify } from 'quasar';

const authHelper = {
  data() {
    return {
      invalidCredentials: false,
    };
  },
  methods: {
    login() {
      this.$store.dispatch('login', { email: this.email.trim(), password: this.password })
        .then(() => {
          this.invalidCredentials = false;
          Notify.create({
            message: 'Bem-vindo!',
            type: 'info',
            icon: 'fas fa-smile-beam',
            timeout: 1000,
          });
          this.$router.push({ name: 'search' });
        })
        .catch(() => {
          this.invalidCredentials = true;
          Notify.create({
            message: 'Email ou senha incorretos ',
            type: 'negative',
          });
          this.errors.add('wrong-credentials', 'Credenciais erradas');
        });
    },
  },
};

export default authHelper;
