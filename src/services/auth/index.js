import HTTP from '../../services/api/http-common';

const LOGIN = 'LOGIN';
const REGISTER = 'REGISTER';
const LOGOUT = 'LOGOUT';
const auth = {
  state() {
    return {
      authenticated: false,
      userId: null,
      token: null,
    };
  },
  mutations: {
    [REGISTER](state, res) {
      const localStorage = state;
      localStorage.token = res.token;
      localStorage.userId = res.id;
      localStorage.authenticated = false;
    },

    [LOGIN](state, res) {
      const localStorage = state;
      localStorage.token = res.token;
      localStorage.userId = res.id;
      localStorage.authenticated = true;
    },

    [LOGOUT](state) {
      const localStorage = state;
      localStorage.token = null;
      localStorage.userId = null;
      localStorage.authenticated = false;
    },
  },
  actions: {
    register({ commit }, credentials) {
      return HTTP.post('register', {
        user: {
          name: credentials.name,
          email: credentials.email,
          password: credentials.password,
        },
      })
        .then((response) => {
          const res = {
            token: response.data.token,
            id: response.data.user_id,
          };

          commit(REGISTER, res);
        });
    },

    login({ commit }, credentials) {
      return HTTP.post('login', {
        user: {
          email: credentials.email,
          password: credentials.password,
        },
      })
        .then((response) => {
          const res = {
            token: response.data.token,
            id: response.data.user_id,
          };

          commit(LOGIN, res);
        })
        .catch((err) => {
          this.errors.add(err);
        });
    },

    logout({ commit }) {
      commit(LOGOUT);
    },
  },
};

export default auth;
