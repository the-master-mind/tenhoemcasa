const SAVE_SEARCH_RESULTS = 'SAVE_SEARCH_RESULTS';
const TOGGLE_FEEDBACK = 'TOGGLE_FEEDBACK';

const recipes = {
  state() {
    return {
      searchResults: [],
      isFeedbackOpened: false,
    };
  },
  mutations: {
    [SAVE_SEARCH_RESULTS](state, response) {
      const localStorage = state;
      localStorage.searchResults = response.searchResults;
    },
    [TOGGLE_FEEDBACK](state, status) {
      const localStorage = state;
      localStorage.isFeedbackOpened = status.isFeedbackOpened;
    },
  },
  actions: {
    saveSearchResults({ commit }, response) {
      commit(SAVE_SEARCH_RESULTS, response);
    },
    toggleFeedback({ commit }, status) {
      commit(TOGGLE_FEEDBACK, status);
    },
  },
};

export default recipes;
