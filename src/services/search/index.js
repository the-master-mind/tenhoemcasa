const SELECTED_ITEMS = 'SELECTED_ITEMS';

const search = {
  state() {
    return {
      selectedItems: ['sal', 'água', 'açúcar', 'óleo', 'fermento'],
    };
  },
  mutations: {
    [SELECTED_ITEMS](state, response) {
      const localStorage = state;
      localStorage.selectedItems = response.selectedItems;
    },
  },
  actions: {
    selectedItems({ commit }, response) {
      commit(SELECTED_ITEMS, response);
    },
  },
};

export default search;
