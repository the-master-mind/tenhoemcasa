
const routes = [
  {
    path: '/',
    name: 'welcome',
    component: () => import('layouts/Welcome.vue'),
    children: [
      { path: '/', name: 'presentation', component: () => import('pages/Presentation.vue') },
      { path: '/about', name: 'about', component: () => import('pages/About.vue') },
      { path: '/terms-of-use', name: 'termsOfUse', component: () => import('pages/TermsOfUse.vue') },
      { path: '/privacy-policy', name: 'privacyPolicy', component: () => import('pages/PrivacyPolicy.vue') },
      { path: 'recipe/:id', name: 'recipe', component: () => import('pages/Recipe.vue') },
      { path: '/search', name: 'search', component: () => import('pages/Search.vue') },
      { path: '/results', name: 'searchResults', component: () => import('pages/SearchResults.vue') },
    ],
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    name: 'Error404',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
