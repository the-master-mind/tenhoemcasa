import Vue from 'vue';
import Vuex from 'vuex';
import auth from '../services/auth/index';
import recipes from '../services/recipes/index';
import search from '../services/search/index';
import VuexPersist from 'vuex-persist';

Vue.use(Vuex);

export default function () {
  const vuexLocalStorage = new VuexPersist({
    key: 'vuex',
    storage: localStorage,
    modules: ['auth', 'recipes', 'search'],
  });

  const Store = new Vuex.Store({
    plugins: [vuexLocalStorage.plugin],
    modules: {
      auth,
      recipes,
      search,
    },
  });

  return Store;
}
